%define debug_package %{nil}
%define _unpackaged_files_terminate_build 0

Name:          fux-terminal
Version:       1.0
Release:       1%{?dist}
Summary:       Fux terminal emulator
License:       GPLv3+
Group:         User Interface/X

URL:           http://fuxproject.org/fux-terminal/fux-terminal.php
Source:        http://fuxproject.org/fux-terminal/1.0/%{name}-%{version}.tar.gz

BuildRequires: dbus-x11
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gtk3-devel >= %{gtk3_version}
BuildRequires: gsettings-desktop-schemas-devel
BuildRequires: vte291-devel >= %{vte_version}
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: systemd
BuildRequires: python
BuildRequires: vte

Requires: dbus-x11
Requires: glib2%{?_isa} >= %{glib2_version}
Requires: gsettings-desktop-schemas
Requires: gtk3%{?_isa} >= %{gtk3_version}
Requires: vte291%{?_isa} >= %{vte_version}
Requires: vte-profile
Requires: python

%description
Fux terminal emulator use GTK+ 2.0.

%prep
%setup -n %{name}-%{version}

%build

%install
mkdir -p $RPM_BUILD_ROOT/usr
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/share
mkdir -p $RPM_BUILD_ROOT/usr/share/fux-terminal
mkdir -p $RPM_BUILD_ROOT/usr/share/applications

cp -f $RPM_BUILD_DIR/%{name}-%{version}/fux-terminal $RPM_BUILD_ROOT/usr/bin
cp -f $RPM_BUILD_DIR/%{name}-%{version}/fux-terminal.py $RPM_BUILD_ROOT/usr/share/fux-terminal
cp -f $RPM_BUILD_DIR/%{name}-%{version}/fux-terminal.png $RPM_BUILD_ROOT/usr/share/fux-terminal
cp -f $RPM_BUILD_DIR/%{name}-%{version}/fux-terminal.desktop $RPM_BUILD_ROOT/usr/share/applications

%files
%defattr(-,root,root)
%doc LICENSE README.md
%dir %{_datadir}/%{name}
%{_bindir}/%{name}
%{_datadir}/%{name}/fux-terminal.py
%{_datadir}/%{name}/fux-terminal.png
%{_datadir}/applications/fux-terminal.desktop

%changelog
* Sat Nov 19 2016 Fuat Bölük <fuat@fuxproject.org> - 1.0-1
- Package created

