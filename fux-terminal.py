#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk, vte, string, os, urllib, sys

def command_on_VTE(command):
    length = len(command)
    terminal.feed_child(command, length)
    terminal2.feed_child(command, length)

def motion_cb(wid, context, x, y, time):
    context.drag_status(gtk.gdk.ACTION_COPY, time)
    return True

def drop_cb(wid, context, x, y, time):
    wid.drag_get_data(context, context.targets[-1], time)
    return True

def got_data_cb(wid, context, x, y, data, info, time):
    fulltext = data.get_text()
    says = fulltext.split("//")
    op = open(".histerm", "w")
    op.write(says[1])
    op.close()
    op = open(".histerm")
    line = op.read().split("\n")
    op.close()

    if line[0] in "/home":
       line[0] = os.environ['HOME']
    elif line[0] in "/trash":
       line[0] = os.environ['HOME']+"/.local/share/Trash/files"
    else:
       pass

    change = urllib.unquote(urllib.unquote(line[0]))
    command_on_VTE(change)


terminal = vte.Terminal()
terminal.drag_dest_set(0, [], 0)
terminal.connect("drag_motion", motion_cb)
terminal.connect("drag_drop", drop_cb)
terminal.connect("drag_data_received", got_data_cb)
terminal.connect("child-exited", exit)
terminal.fork_command("/bin/bash")

terminal2 = vte.Terminal()
terminal2.drag_dest_set(0, [], 0)
terminal2.connect("drag_motion", motion_cb)
terminal2.connect("drag_drop", drop_cb)
terminal2.connect("drag_data_received", got_data_cb)
terminal2.connect("child-exited", exit)
terminal2.fork_command("/bin/bash")

terminal3 = vte.Terminal()
terminal3.drag_dest_set(0, [], 0)
terminal3.connect("drag_motion", motion_cb)
terminal3.connect("drag_drop", drop_cb)
terminal3.connect("drag_data_received", got_data_cb)
terminal3.connect("child-exited", exit)
terminal3.fork_command("/bin/bash")

terminal4 = vte.Terminal()
terminal4.drag_dest_set(0, [], 0)
terminal4.connect("drag_motion", motion_cb)
terminal4.connect("drag_drop", drop_cb)
terminal4.connect("drag_data_received", got_data_cb)
terminal4.connect("child-exited", exit)
terminal4.fork_command("/bin/bash")

win = gtk.Window()

winh = gtk.HBox()
winh2 = gtk.HBox()
winv = gtk.VBox()
win.connect("delete-event", gtk.main_quit)
if sys.argv[len(sys.argv)-1] == "2":
  win.add(winh)    
  winh.pack_start(terminal2)
  winh.pack_start(gtk.Label(":"))
  winh.pack_start(terminal)
  win.add(winh)
if sys.argv[len(sys.argv)-1] == "4":
  winv.pack_start(winh)
  winv.pack_start(winh2)    
  winh.pack_start(terminal2)
  winh.pack_start(terminal) 
  winh2.pack_start(terminal3)
  winh2.pack_start(terminal4)
  win.add(winv)
else:
  win.add(terminal)
win.set_title("Fux Terminal")
win.set_position(gtk.WIN_POS_CENTER_ALWAYS)
win.set_size_request(800,400)
win.show_all()
if "-f" in sys.argv:
  win.fullscreen()
gtk.main()

